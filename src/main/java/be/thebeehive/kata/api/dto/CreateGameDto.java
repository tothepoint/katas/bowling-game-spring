package be.thebeehive.kata.api.dto;

public record CreateGameDto(
        String name
) {
}
