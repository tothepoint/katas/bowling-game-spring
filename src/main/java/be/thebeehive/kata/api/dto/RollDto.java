package be.thebeehive.kata.api.dto;

public record RollDto(
        int pins
) {
}
